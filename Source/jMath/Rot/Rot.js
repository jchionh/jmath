// create namespace
jmath = jmath || {};

//---------------------------------------------------------------------------------------
// ctor for Rot
jmath.Rot = function(angle) {
	// default value
	angle = angle || 0.0;
	this.Set(angle);
};

//---------------------------------------------------------------------------------------
//Set
jmath.Rot.prototype.Set = function(angle) {
	this.s = Math.sin(angle);
	this.c = Math.cos(angle);
};

//---------------------------------------------------------------------------------------
//SetIdentity
jmath.Rot.prototype.SetIdentity = function() {
	this.s = 0.0;
	this.c = 1.0;
};

//---------------------------------------------------------------------------------------
//GetAngle
jmath.Rot.prototype.GetAngle = function() {
	return Math.atan2(this.s, this.c);
};

//---------------------------------------------------------------------------------------
//GetXAxis
jmath.Rot.prototype.GetXAxis = function() {
	return new jmath.Vec2(this.c, this.s);
};

//---------------------------------------------------------------------------------------
//GetYAxis
jmath.Rot.prototype.GetYAxis = function() {
	return new jmath.Vec2(-this.s, this.c);
};

