// create the main jmath namepsace
var jmath = jmath || {};

jmath.gPrevTime = 0;
jmath.gTitleElement = null;

// machine epsilon
jmath.epsilon = 1.192092896e-07;

//---------------------------------------------------------------------------------------
//IsValid
jmath.IsValid = function(x) {
	return isFinite(x);
};

//---------------------------------------------------------------------------------------
//Vec2Dot
/// Perform the dot product on two vectors. Vec2
jmath.Vec2Dot = function(a, b) {
	return (a.x * b.x) + (a.y * b.y);
};

//---------------------------------------------------------------------------------------
//Vec3Dot
/// Perform the dot product on two vectors. Vec3
jmath.Vec3Dot = function(a, b) {
	return a.x * b.x + a.y * b.y + a.z * b.z;
};

//---------------------------------------------------------------------------------------
//Vec2Cross
/// Perform the cross product on two vectors. In 2D this produces a scalar.
jmath.Vec2Cross = function(a, b) {
	return (a.x * b.y) - (a.y * b.x);
};

//---------------------------------------------------------------------------------------
//Vec3Cross
/// Perform the cross product on two vectors.
jmath.Vec3Cross = function(a, b) {
	return new jmath.Vec3((a.y * b.z) - (a.z * b.y), (a.z * b.x) - (a.x * b.z), (a.x * b.y) - (a.y * b.x));
};

//---------------------------------------------------------------------------------------
//Vec2MulRot
/// multiply the Vec2 with a rotator
jmath.Vec2MulRot = function(rot, vec2) {
	return new jmath.Vec2(rot.c * vec2.x - rot.s * vec2.y, rot.s * vec2.x + rot.c * vec2.y);
};
