// create namespace
jmath = jmath || {};

//---------------------------------------------------------------------------------------
//ctor for Vec2
jmath.Vec2 = function(x, y) {
	// default values
	x = x || 0.0;
	y = y || 0.0;
	
	this.Set(x, y);
};

//---------------------------------------------------------------------------------------
//IsValid
jmath.Vec2.prototype.IsValid = function() {
	return jmath.IsValid(this.x) && jmath.IsValid(this.y);
};

//---------------------------------------------------------------------------------------
//SetZero
jmath.Vec2.prototype.SetZero = function() {
	this.x = 0.0;
	this.y = 0.0;
};

//---------------------------------------------------------------------------------------
//Set
jmath.Vec2.prototype.Set = function(x, y) {
	this.x = x;
	this.y = y;
};

//---------------------------------------------------------------------------------------
//SetV
jmath.Vec2.prototype.SetV = function(v) {
	this.x = v.x;
	this.y = v.y;
};

//---------------------------------------------------------------------------------------
//Negative
jmath.Vec2.prototype.Negative = function() {
	return new jmath.Vec2(-this.x, -this.y);
};

//---------------------------------------------------------------------------------------
//Copy
jmath.Vec2.prototype.Copy = function() {
	return new jmath.Vec2(this.x, this.y);
};

//---------------------------------------------------------------------------------------
//Add - add a vector to this vector
jmath.Vec2.prototype.Add = function(v) {
	this.x += v.x;
	this.y += v.y;
};

//---------------------------------------------------------------------------------------
//Subtract - sub a vector from this vector
jmath.Vec2.prototype.Subtract = function(v) {
	this.x -= v.x;
	this.y -= v.y;
};

//---------------------------------------------------------------------------------------
//Multiply - multiply a scalar to this vector
jmath.Vec2.prototype.Multiply = function(a) {
	this.x *= a;
	this.y *= a;
};

//---------------------------------------------------------------------------------------
//Length - get the length of this vector
jmath.Vec2.prototype.Length = function() {
	return Math.sqrt((this.x * this.x) + (this.y * this.y));
};

//---------------------------------------------------------------------------------------
//LengthSquared - get the length of this vector
jmath.Vec2.prototype.LengthSquared = function() {
	return (this.x * this.x) + (this.y * this.y);
};

//---------------------------------------------------------------------------------------
//Normalize - Convert this vector into a unit vector. Returns the length.
jmath.Vec2.prototype.Normalize = function() {
	var length = this.Length();
	
	if (length < jmath.epsilon)
	{
		return 0.0;
	}
	
	var invLength = 1.0 / length;
	this.x *= invLength;
	this.y *= invLength;
	return length;
};

