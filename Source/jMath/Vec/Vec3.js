// create namespace
jmath = jmath || {};

//---------------------------------------------------------------------------------------
// ctor for Vec3
jmath.Vec3 = function(x, y, z) {
	// default values
	x = x || 0.0;
	y = y || 0.0;
	z = z || 0.0;
	
	this.Set(x, y, z);
};

//---------------------------------------------------------------------------------------
//SetZero
jmath.Vec3.prototype.SetZero = function() {
	this.x = 0.0;
	this.y = 0.0;
	this.z = 0.0;
};

//---------------------------------------------------------------------------------------
//Set
jmath.Vec3.prototype.Set = function(x, y, z) {
	this.x = x;
	this.y = y;
	this.z = z;
};

//---------------------------------------------------------------------------------------
//SetV
jmath.Vec3.prototype.SetV = function(v) {
	this.x = v.x;
	this.y = v.y;
	this.z = v.z;
};

//---------------------------------------------------------------------------------------
//Negative
jmath.Vec3.prototype.Negative = function() {
	return new jmath.Vec3(-this.x, -this.y, -this.z);
};

//---------------------------------------------------------------------------------------
//Copy
jmath.Vec3.prototype.Copy = function() {
	return new jmath.Vec3(this.x, this.y, this.z);
};

//---------------------------------------------------------------------------------------
//Add - add a vector to this vector
jmath.Vec3.prototype.Add = function(v) {
	this.x += v.x;
	this.y += v.y;
	this.z += v.z;
};

//---------------------------------------------------------------------------------------
//Subtract - sub a vector from this vector
jmath.Vec3.prototype.Subtract = function(v) {
	this.x -= v.x;
	this.y -= v.y;
	this.z -= v.z;
};

//---------------------------------------------------------------------------------------
//Multiply - multiply a scalar to this vector
jmath.Vec3.prototype.Multiply = function(a) {
	this.x *= a;
	this.y *= a;
	this.z *= a;
};
