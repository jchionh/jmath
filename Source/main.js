
// the main init function
function mainInit() {
	
	// shim layer with setTimeout fallback
    window.requestAnimationFrame = (function(){
      return  window.requestAnimationFrame       || 
              window.webkitRequestAnimationFrame || 
              window.mozRequestAnimationFrame    || 
              window.oRequestAnimationFrame      || 
              window.msRequestAnimationFrame     || 
              function(/* function */ callback, /* DOMElement */ element){
                window.setTimeout(callback, 1000 / 60);
              };
    })();
    
    // get refernce to the title element, just to update fps etc
    jmath.gTitleElement = document.getElementsByTagName('title')[0];
    
    // testing
    var v2 = new jmath.Vec2();
    v2.Normalize();
    
	window.requestAnimationFrame(mainLoop);
}


function mainLoop(time) {
	// record the delta time for our main loop
	var deltaTime = time - jmath.gPrevTime;
	jmath.gTitleElement.innerHTML = '(' + deltaTime + 'ms) jMath';
	
	// do something with our delta time
	//console.log("delta: " + deltaTime);
	
	// store our time now
	jmath.gPrevTime = time;
	
	// in the end call requestAnimationFrame again
	window.requestAnimationFrame(mainLoop);
}


